
package Entidades;

import java.util.Date;
import java.util.Objects;

public class Vacina {
    
    private Integer id;
    private Date datatratamento;
    private String tipotratamento;
    private String detalhe;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDatatratamento() {
        return datatratamento;
    }

    public void setDatatratamento(Date datatratamento) {
        this.datatratamento = datatratamento;
    }

    public String getTipotratamento() {
        return tipotratamento;
    }

    public void setTipotratamento(String tipotratamento) {
        this.tipotratamento = tipotratamento;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vacina other = (Vacina) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
