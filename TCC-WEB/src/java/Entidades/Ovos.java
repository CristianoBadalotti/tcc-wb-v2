
package Entidades;

import java.util.Date;
import java.util.Objects;

public class Ovos {
    private int quantidade_ovos;
    private String qualidade_ovos;
    private Date data_ponhagem_ovos;
    private boolean incubacao_ovos = false;
    private String lote;
    private Integer id_ovos;
    private String tipoAve;

    public String getTipoAve() {
        return tipoAve;
    }

    public void setTipoAve(String tipoAve) {
        this.tipoAve = tipoAve;
    }
    
    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Ovos() {
        
    }
    
    public Date getData_ponhagem_ovos() {
        return data_ponhagem_ovos;
    }

    public String getQualidade_ovos() {
        return qualidade_ovos;
    }

    public int getQuantidade_ovos() {
        return quantidade_ovos;
    }
    
    public boolean getIncubacao_ovos() {
        return incubacao_ovos;
    }

    public void setData_ponhagem_ovos(Date data_ponhagem_ovos) {
        this.data_ponhagem_ovos = data_ponhagem_ovos;
    }

    public void setIncubacao_ovos(boolean incubacao_ovos) {
        this.incubacao_ovos = incubacao_ovos;
    }

    public void setQualidade_ovos(String qualidade_ovos) {
        this.qualidade_ovos = qualidade_ovos;
    }

    public void setQuantidade_ovos(int quantidade_ovos) {
        this.quantidade_ovos = quantidade_ovos;
    }

    public Integer getId_ovos() {
        return id_ovos;
    }

    public void setId_ovos(Integer id_ovos) {
        this.id_ovos = id_ovos;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.id_ovos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ovos other = (Ovos) obj;
        if (!Objects.equals(this.id_ovos, other.id_ovos)) {
            return false;
        }
        return true;
    }
}
