
package Entidades;

import java.util.Date;
import java.util.Objects;

public class GranjaOvos {
    
    private Integer id;
    private int quantidadeaves;
    private int numeroovos;
    private String comentario;
    private int maximoaves;
    private Date dataentrada;
    private Date datasaida;
    private String tipoAve;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidadeaves() {
        return quantidadeaves;
    }

    public void setQuantidadeaves(int quantidadeaves) {
        this.quantidadeaves = quantidadeaves;
    }

    public int getNumeroovos() {
        return numeroovos;
    }

    public void setNumeroovos(int numeroovos) {
        this.numeroovos = numeroovos;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getMaximoaves() {
        return maximoaves;
    }

    public void setMaximoaves(int maximoaves) {
        this.maximoaves = maximoaves;
    }

    public Date getDataentrada() {
        return dataentrada;
    }

    public void setDataentrada(Date dataentrada) {
        this.dataentrada = dataentrada;
    }

    public Date getDatasaida() {
        return datasaida;
    }

    public void setDatasaida(Date datasaida) {
        this.datasaida = datasaida;
    }

    public String getTipoAve() {
        return tipoAve;
    }

    public void setTipoAve(String tipoAve) {
        this.tipoAve = tipoAve;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GranjaOvos other = (GranjaOvos) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}
