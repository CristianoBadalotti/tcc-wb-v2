
package Entidades;

import java.util.Date;
import java.util.Objects;

public class Incubatorio {
    
    private Integer id;
    private String loteovos;
    private int temperatura;
    private int umidade;
    private int tempochocar;
    private Date datainicio;
    private int mortalidade;
    private String tipoAve;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoteovos() {
        return loteovos;
    }

    public void setLoteovos(String loteovos) {
        this.loteovos = loteovos;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public int getUmidade() {
        return umidade;
    }

    public void setUmidade(int umidade) {
        this.umidade = umidade;
    }

    public int getTempochocar() {
        return tempochocar;
    }

    public void setTempochocar(int tempochocar) {
        this.tempochocar = tempochocar;
    }

    public Date getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(Date datainicio) {
        this.datainicio = datainicio;
    }

    public int getMortalidade() {
        return mortalidade;
    }

    public void setMortalidade(int mortalidade) {
        this.mortalidade = mortalidade;
    }

    public String getTipoAve() {
        return tipoAve;
    }

    public void setTipoAve(String tipoAve) {
        this.tipoAve = tipoAve;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Incubatorio other = (Incubatorio) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
