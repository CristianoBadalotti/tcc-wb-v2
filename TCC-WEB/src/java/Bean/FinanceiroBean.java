
package Bean;

import Entidades.Financeiro;
import DAO.FinanceiroDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class FinanceiroBean extends CrudBean<Financeiro, FinanceiroDAO>{

    private FinanceiroDAO finDAO;
    
    @Override
    public FinanceiroDAO getDAO() {
        if (finDAO == null) {
            finDAO = new FinanceiroDAO();
        }
        return finDAO;
    }

    @Override
    public Financeiro criarNovaEntidade() {
        return new Financeiro();
    }
    
}
