
package Bean;

import Entidades.Racao;
import DAO.RacaoDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class RacaoBean extends CrudBean<Racao, RacaoDAO>{

    private RacaoDAO raDAO;
    
    @Override
    public RacaoDAO getDAO() {
        if (raDAO == null) {
            raDAO = new RacaoDAO();
        }
        return raDAO;
    }

    @Override
    public Racao criarNovaEntidade() {
        return new Racao();
    }  
}
