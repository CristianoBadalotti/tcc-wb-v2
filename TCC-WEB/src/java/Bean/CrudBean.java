
package Bean;

import DAO.InterfaceCrudDAO;
import Controle.ErroSistema;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public abstract class CrudBean<E, D extends InterfaceCrudDAO> {
    
    private String estadoTela = "buscar";
    private E entidade;
    private List<E> entidades;
    
    public CrudBean() {
        entidade = criarNovaEntidade();
    }
    
    public void newEnt() {
        entidade = criarNovaEntidade();
        mudarParaInseri();
    }
    
    public void clear() {
        entidade = criarNovaEntidade();
    }
    
    public void returnPes() {
        clear();
        mudarParaBusca();
    }
    
    public void save() {
        try {
            getDAO().salvar(entidade);
            entidade = criarNovaEntidade();
            adicionarMensagem("Salvo com sucesso!", FacesMessage.SEVERITY_INFO);
            mudarParaBusca();
        } catch (ErroSistema ex) {
            Logger.getLogger(CrudBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }
    
    public void change(E entidade) {
        this.entidade = entidade;
        mudarParaEdita();
    }
    
    public void delete(E entidade) {
        try {
            getDAO().deletar(entidade);
            entidades.remove(entidade);
            adicionarMensagem("Deletado com sucesso!", FacesMessage.SEVERITY_INFO);
        } catch (ErroSistema ex) {
            Logger.getLogger(CrudBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }
    
    public void search() {
        if (!isBusca()) {
            mudarParaBusca();
            return;
        }
        try {
            entidades = getDAO().buscar(entidade);
            if (entidades == null || entidades.size() < 1) {
                adicionarMensagem("Não existe nada cadastrado!", FacesMessage.SEVERITY_WARN);
            }
        } catch (ErroSistema ex) {
            Logger.getLogger(CrudBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
        
    }
    
    public void adicionarMensagem(String mensagem, FacesMessage.Severity tipoErro) {
        FacesMessage fm = new FacesMessage(tipoErro, mensagem, null);
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }
    
    public E getEntidade() {
        return entidade;
    }

    public void setEntidade(E entidade) {
        this.entidade = entidade;
    }

    public List<E> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<E> entidades) {
        this.entidades = entidades;
    }
    
    public abstract D getDAO();
    
    public abstract E criarNovaEntidade();
    
    public boolean isInseri() {
        return "inserir".equals(estadoTela);
    }
    
    public boolean isEdita() {
        return "editar".equals(estadoTela);
    }
    
    public boolean isBusca() {
        boolean bc = "buscar".equals(estadoTela);
        return bc;
    }
    
    public void mudarParaInseri() {
        estadoTela = "inserir";
    }
    
    public void mudarParaEdita() {
        estadoTela = "editar";
    }
    
    public void mudarParaBusca() {
        estadoTela = "buscar";
    }
}
