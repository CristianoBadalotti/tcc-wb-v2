
package Bean;

import Entidades.GranjaCorte;
import DAO.GranjaCorteDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class GranjaCorteBean extends CrudBean<GranjaCorte, GranjaCorteDAO>{

    private GranjaCorteDAO gcDAO;
    
    @Override
    public GranjaCorteDAO getDAO() {
        if (gcDAO == null) {
            gcDAO = new GranjaCorteDAO();
        }
        return gcDAO;
    }

    @Override
    public GranjaCorte criarNovaEntidade() {
        return new GranjaCorte();
    }
    
}
