
package Bean;

import Entidades.Usuario;
import Bean.CrudBean;
import DAO.UsuarioDAO;
import Controle.ErroSistema;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class UsuarioBean extends CrudBean<Usuario, UsuarioDAO>{

    private UsuarioDAO usuDAO;
    
    @Override
    public UsuarioDAO getDAO() {
        if (usuDAO == null) {
            usuDAO = new UsuarioDAO();
        }
        return usuDAO;
    }

    @Override
    public Usuario criarNovaEntidade() {
        return new Usuario();
    }
}
