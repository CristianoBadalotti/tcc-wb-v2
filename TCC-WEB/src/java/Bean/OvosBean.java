package Bean;

import Entidades.Ovos;
import DAO.OvosDAO;
import Controle.ErroSistema;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class OvosBean extends CrudBean<Ovos, OvosDAO> {

    private OvosDAO ovDAO;

    @Override
    public OvosDAO getDAO() {
        if (ovDAO == null) {
            ovDAO = new OvosDAO();
        }
        return ovDAO;
    }

    @Override
    public Ovos criarNovaEntidade() {
        return new Ovos();
    }
    
    public List<String> buscaListaString() {
        try {
            return getDAO().buscarListaLote();
        } catch (ErroSistema ex) {
            Logger.getLogger(OvosBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
        return null;
    }
}
