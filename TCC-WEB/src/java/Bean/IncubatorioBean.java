
package Bean;

import Entidades.Incubatorio;
import DAO.IncubatorioDAO;
import Controle.ErroSistema;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class IncubatorioBean extends CrudBean<Incubatorio, IncubatorioDAO>{

    private IncubatorioDAO incDAO;
    
    @Override
    public IncubatorioDAO getDAO() {
        if (incDAO == null) {
            incDAO = new IncubatorioDAO();
        }
        return incDAO;
    }

    @Override
    public Incubatorio criarNovaEntidade() {
        return new Incubatorio();
    }
}
