
package Bean;

import Entidades.Vacina;
import Bean.CrudBean;
import DAO.VacinaDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class VacinaBean extends CrudBean<Vacina, VacinaDAO>{

    private VacinaDAO vacDAO;
    
    @Override
    public VacinaDAO getDAO() {
        if (vacDAO == null) {
            vacDAO = new VacinaDAO();
        }
        return vacDAO;
    }

    @Override
    public Vacina criarNovaEntidade() {
        return new Vacina();
    } 
}
