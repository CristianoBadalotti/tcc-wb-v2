
package Bean;

import Entidades.GranjaOvos;
import DAO.GranjaOvosDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class GranjaOvosBean extends CrudBean<GranjaOvos, GranjaOvosDAO>{

    private GranjaOvosDAO goDAO;
    
    @Override
    public GranjaOvosDAO getDAO() {
        if (goDAO == null) {
            goDAO = new GranjaOvosDAO();
        }
        return goDAO;
    }

    @Override
    public GranjaOvos criarNovaEntidade() {
        return new GranjaOvos();
    }
}
