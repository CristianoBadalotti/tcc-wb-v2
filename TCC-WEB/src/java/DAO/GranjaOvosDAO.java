package DAO;

import Entidades.GranjaOvos;
import Controle.Conexao;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GranjaOvosDAO implements InterfaceCrudDAO<GranjaOvos> {

    @Override
    public void salvar(GranjaOvos entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId() == null) {
                prepadador = con.prepareStatement("WITH GR_KEY AS (INSERT INTO GRANJA(IDTIPOAVE, QUANTIDADE, COMENTARIO, MAXAVES, "
                        + "DATAENTRADA, DATASAIDA) VALUES (?, ?, ?, ?, ?, ?) RETURNING IDGRANJA) "
                        + "INSERT INTO GRANJAPOSTURA(IDGRANJA, NUMEROOVOS) SELECT GR_KEY.IDGRANJA, ? FROM GR_KEY;");
                int idTipoAve = new TipoAvesDAO().buscarID(entidade.getTipoAve());
                prepadador.setInt(1, idTipoAve);
                prepadador.setInt(2, entidade.getQuantidadeaves());
                prepadador.setString(3, entidade.getComentario());
                prepadador.setInt(4, entidade.getMaximoaves());
                prepadador.setDate(5, new Date(entidade.getDataentrada().getTime()));
                prepadador.setDate(6, new Date(entidade.getDatasaida().getTime()));
                prepadador.setInt(7, entidade.getNumeroovos());
            } else {
                prepadador = con.prepareStatement("WITH GR_KEY AS (UPDATE GRANJAPOSTURA SET NUMEROOVOS = ? WHERE IDGRANJAPOSTURA = ? RETURNING *)\n"
                        + "UPDATE GRANJA SET IDTIPOAVE = ?, QUANTIDADE = ?, COMENTARIO = ?, MAXAVES = ?, DATAENTRADA = ?, DATASAIDA = ? \n"
                        + "WHERE IDGRANJA IN (SELECT IDGRANJA FROM GR_KEY)");
                prepadador.setInt(1, entidade.getNumeroovos());
                prepadador.setInt(2, entidade.getId());
                int idTipoAve = new TipoAvesDAO().buscarID(entidade.getTipoAve());
                prepadador.setInt(3, idTipoAve);
                prepadador.setInt(4, entidade.getQuantidadeaves());
                prepadador.setString(5, entidade.getComentario());
                prepadador.setInt(6, entidade.getMaximoaves());
                prepadador.setDate(7, new Date(entidade.getDataentrada().getTime()));
                prepadador.setDate(8, new Date(entidade.getDatasaida().getTime()));
            }

            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar granja de corte!", ex);
        }
    }

    @Override
    public void deletar(GranjaOvos entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("WITH DL_KEY AS (DELETE FROM GRANJAPOSTURA WHERE IDGRANJAPOSTURA = ? RETURNING IDGRANJA) \n"
                    + "DELETE FROM GRANJA USING DL_KEY WHERE GRANJA.IDGRANJA = DL_KEY.IDGRANJA");
            prepadador.setInt(1, entidade.getId());
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar granja postura!", ex);
        }
    }

    @Override
    public List<GranjaOvos> buscar(GranjaOvos entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM GRANJAPOSTURA AS A JOIN GRANJA AS B ON (A.IDGRANJA = B.IDGRANJA)\n"
                    + "JOIN TIPOAVES AS C ON (B.IDTIPOAVE = C.ID_TIPOAVES) WHERE 1 = 1");
            ResultSet rs = prepadador.executeQuery();
            List<GranjaOvos> lista = new ArrayList<>();
            while (rs.next()) {
                GranjaOvos go = new GranjaOvos();
                go.setId(rs.getInt("IDGRANJAPOSTURA"));
                go.setNumeroovos(rs.getInt("NUMEROOVOS"));
                go.setQuantidadeaves(rs.getInt("QUANTIDADE"));
                go.setComentario(rs.getString("COMENTARIO"));
                go.setMaximoaves(rs.getInt("MAXAVES"));
                go.setDataentrada(rs.getDate("DATAENTRADA"));
                go.setDatasaida(rs.getDate("DATASAIDA"));
                go.setTipoAve(rs.getString("TA_NOME"));
                lista.add(go);
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por granja de postura!", ex);
        }
    }

}
