
package DAO;

import Entidades.TipoAves;
import Controle.Conexao;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class TipoAvesDAO {
    
    public List<TipoAves> buscarTodos(TipoAves ta) throws ErroSistema {
        List<TipoAves> lista = new ArrayList<>();
        
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM TIPOAVES");
            ResultSet rs = prepadador.executeQuery();
            
            while (rs.next()) {                
                TipoAves ta1 = new TipoAves();
                ta1.setId(rs.getInt("ID_TIPOAVES"));
                ta1.setNome(rs.getString("TA_NOME"));
                lista.add(ta1);
            }
            
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar todos tipos de aves!", ex);
        }
        
        return lista;
    }
    
    public List<String> buscarLista() throws ErroSistema {
        List<String> lista = new ArrayList<>();
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM TIPOAVES");
            ResultSet rs = prepadador.executeQuery();
            
            while (rs.next()) {  
                lista.add(rs.getString("TA_NOME"));
            }
            
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar lista de nomes de aves!", ex);
        }
        
        return lista;
    }
    
    public int buscarID(String name) throws ErroSistema {           
        int id = -1;  
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM TIPOAVES WHERE TA_NOME = '" + name + "'");
            ResultSet rs = prepadador.executeQuery();
      
            while (rs.next()) {  
               id = rs.getInt("ID_TIPOAVES");
            }
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar ID tipo aves!", ex);
        }
        return id;
    }
    
    public String buscarName(int id) throws ErroSistema {        
        String name = "";
        
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM TIPOAVES WHERE ID_TIPOAVES = " + id );
            ResultSet rs = prepadador.executeQuery();
      
            while (rs.next()) {  
               name = rs.getString("TA_NOME");
            }
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar Nome tipo aves!", ex);
        }
        return name;
    }
}
