
package DAO;

import Entidades.Ovos;
import Controle.Conexao;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OvosDAO implements InterfaceCrudDAO<Ovos>{
    
    @Override
    public void salvar(Ovos entidade) throws ErroSistema {      
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId_ovos() == null) {
                prepadador = con.prepareStatement("INSERT INTO OVOS (OV_LOTE, OV_DATAPONHAGEM, OV_QUANTIDADE, OV_INCUBACAO, ID_TIPOAVE, OV_QUALIDADE) VALUES (?, ?, ?, ?, ?, ?)");
            } else {
                prepadador = con.prepareStatement("UPDATE OVOS  SET  OV_LOTE = ?, OV_DATAPONHAGEM = ?, OV_QUANTIDADE = ?, OV_INCUBACAO = ?, ID_TIPOAVE = ?, OV_QUALIDADE = ? WHERE ID_OVOS = ?");
                prepadador.setInt(7, entidade.getId_ovos());
            }
            
            prepadador.setString(1, entidade.getLote());
            prepadador.setDate(2, new Date(entidade.getData_ponhagem_ovos().getTime()));
            prepadador.setInt(3, entidade.getQuantidade_ovos());
            prepadador.setBoolean(4, entidade.getIncubacao_ovos());
            int idTipoAve = new TipoAvesDAO().buscarID(entidade.getTipoAve());
            prepadador.setInt(5, idTipoAve);
            prepadador.setString(6, entidade.getQualidade_ovos());
            
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar lote ovos!", ex);
        }
    }
    
    @Override
    public void deletar(Ovos entidade) throws ErroSistema {          
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("DELETE FROM OVOS WHERE ID_OVOS = ?");
            prepadador.setInt(1, entidade.getId_ovos());         
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar ovos!", ex);
        }
    }
    
    @Override
    public List<Ovos> buscar(Ovos entidade) throws ErroSistema {      
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM OVOS AS A JOIN TIPOAVES AS B ON (A.ID_TIPOAVE = B.ID_TIPOAVES) WHERE 1 = 1");
            ResultSet rs = prepadador.executeQuery();           
            List<Ovos> lista = new ArrayList<>();
            while (rs.next()) {                
                Ovos ov2 = new Ovos();
                ov2.setId_ovos(rs.getInt("ID_OVOS"));
                ov2.setLote(rs.getString("OV_LOTE"));
                ov2.setData_ponhagem_ovos(rs.getDate("OV_DATAPONHAGEM"));
                ov2.setQuantidade_ovos(rs.getInt("OV_QUANTIDADE"));
                ov2.setIncubacao_ovos(rs.getBoolean("OV_INCUBACAO"));
                ov2.setTipoAve(rs.getString("TA_NOME"));
                ov2.setQualidade_ovos(rs.getString("OV_QUALIDADE"));
                lista.add(ov2);
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por ovos!", ex);
        }
    }
    public List<String> buscarListaLote() throws ErroSistema {
        List<String> lista = new ArrayList<>();
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM OVOS");
            ResultSet rs = prepadador.executeQuery();
            
            while (rs.next()) {  
                lista.add(rs.getString("OV_LOTE"));
            }
            
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar lista de nomes de aves!", ex);
        }
        
        return lista;
    }
    
    public int buscarID(String lote) throws ErroSistema {           
        int id = -1;  
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM OVOS WHERE OV_LOTE = '" + lote + "'");
            ResultSet rs = prepadador.executeQuery();
      
            while (rs.next()) {  
               id = rs.getInt("ID_OVOS");
            }
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar ID tipo aves!", ex);
        }
        return id;
    }
}
