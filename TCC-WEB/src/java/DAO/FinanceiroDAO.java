
package DAO;

import Controle.Conexao;
import Entidades.Financeiro;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FinanceiroDAO implements InterfaceCrudDAO<Financeiro> {

    @Override
    public void salvar(Financeiro entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId() == null) {
                prepadador = con.prepareStatement("INSERT INTO FINANCEIRO(NOMEITEM, VALOR, DETALHE, ENTRADASAIDA) VALUES (?, ?, ?, ?)");
            } else {
                prepadador = con.prepareStatement("UPDATE FINANCEIRO SET NOMEITEM = ?, VALOR = ?, DETALHE = ?, ENTRADASAIDA = ? WHERE IDFINANCEIRO = ?");
                prepadador.setInt(5, entidade.getId());
            }
            
            prepadador.setString(1, entidade.getNome());
            prepadador.setDouble(2, entidade.getValor());
            prepadador.setString(3, entidade.getDetalhe());
            String es;
                if (entidade.getEntrasaida().equals("Entrada")) {
                    es = "E";
                } else {
                    es = "S";
                }
            prepadador.setString(4, es); 
            
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar item!", ex);
        }
    }

    @Override
    public void deletar(Financeiro entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("DELETE FROM FINANCEIRO WHERE IDFINANCEIRO = ?");
            prepadador.setInt(1, entidade.getId());         
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar item!", ex);
        }
    }

    @Override
    public List<Financeiro> buscar(Financeiro entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM FINANCEIRO WHERE 1 = 1");
            ResultSet rs = prepadador.executeQuery();           
            List<Financeiro> lista = new ArrayList<>();
            while (rs.next()) {                
                Financeiro obj = new Financeiro();    
                obj.setId(rs.getInt("IDFINANCEIRO"));//idfinanceiro, nomeitem, valor, detalhe, entradasaida
                obj.setDetalhe(rs.getString("DETALHE"));
                String es;
                if (rs.getString("ENTRADASAIDA").equals("E")) {
                    es = "Entrada";
                } else {
                    es = "Saida";
                }
                obj.setEntrasaida(es);
                obj.setNome(rs.getString("NOMEITEM"));
                obj.setValor(rs.getDouble("VALOR"));
                lista.add(obj);
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por itens!", ex);
        }
    }

 
    
}
