
package DAO;

import Entidades.Vacina;
import Controle.Conexao;
import DAO.InterfaceCrudDAO;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VacinaDAO implements InterfaceCrudDAO<Vacina>{

    @Override
    public void salvar(Vacina entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId() == null) {
                prepadador = con.prepareStatement("INSERT INTO VACINA(IDGRANJA, DATATRATAMENTO, TIPOTRATAMENTO, DETALHE) VALUES (?, ?, ?, ?)");
            } else {
                prepadador = con.prepareStatement("UPDATE VACINA SET IDGRANJA = ?, DATATRATAMENTO = ?, TIPOTRATAMENTO = ?, DETALHE = ? WHERE IDVACINA = ?");
                prepadador.setInt(5, entidade.getId());
            }
            
            prepadador.setInt(1, 1); // falta implementação
            prepadador.setDate(2, new Date(entidade.getDatatratamento().getTime()));
            prepadador.setString(3, entidade.getTipotratamento());
            prepadador.setString(4, entidade.getDetalhe());
            
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar lote ovos!", ex);
        }
    }

    @Override
    public void deletar(Vacina entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("DELETE FROM VACINA WHERE IDVACINA = ?");
            prepadador.setInt(1, entidade.getId());         
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar tratamento!", ex);
        }
    }

    @Override
    public List<Vacina> buscar(Vacina entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM VACINA WHERE 1 = 1");
            ResultSet rs = prepadador.executeQuery();           
            List<Vacina> lista = new ArrayList<>();
            while (rs.next()) {                
                Vacina obj = new Vacina(); //idvacina, idgranja, datatratamento, tipotratamento, detalhe
                obj.setId(rs.getInt("IDVACINA"));
                obj.setDatatratamento(rs.getDate("DATATRATAMENTO"));
                obj.setDetalhe(rs.getString("DETALHE"));
                obj.setTipotratamento(rs.getString("TIPOTRATAMENTO"));
                lista.add(obj);
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por tratamentos!", ex);
        }
    }
}
