package DAO;

import Entidades.Usuario;
import Controle.Conexao;
import DAO.InterfaceCrudDAO;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioDAO implements InterfaceCrudDAO<Usuario> {

    @Override
    public void salvar(Usuario entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId() == null) {
                prepadador = con.prepareStatement("INSERT INTO USUARIO (USUARIO, SENHA) VALUES (?, ?)");
            } else {
                prepadador = con.prepareStatement("UPDATE USUARIO  SET  USUARIO = ?, SENHA = ? WHERE ID = ?");
                prepadador.setInt(3, entidade.getId());
            }
            
            prepadador.setString(1, entidade.getLogin());
            prepadador.setString(2, entidade.getSenha());

            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar usuario!", ex);
        }
    }

    @Override
    public void deletar(Usuario entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("DELETE FROM USUARIO WHERE ID = ?");
            prepadador.setInt(1, entidade.getId());         
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar usuario!", ex);
        }
    }

    @Override
    public List<Usuario> buscar(Usuario entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM USUARIO");
            ResultSet rs = prepadador.executeQuery();           
            List<Usuario> lista = new ArrayList<>();
            while (rs.next()) {                
                Usuario ent1 = new Usuario();
                ent1.setId(rs.getInt("ID"));
                ent1.setLogin(rs.getString("USUARIO"));
                ent1.setSenha(rs.getString("SENHA"));
                lista.add(ent1);
                if (rs.getString("USUARIO").equals(entidade.getLogin()) || rs.getString("SENHA").equals(entidade.getSenha())) {
                    entidade.setEstaLogado(true);
                }
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por ovos!", ex);
        }
    }
}
