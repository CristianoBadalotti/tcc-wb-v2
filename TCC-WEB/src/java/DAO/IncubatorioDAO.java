package DAO;

import Entidades.Incubatorio;
import Controle.Conexao;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IncubatorioDAO implements InterfaceCrudDAO<Incubatorio> {

    @Override
    public void salvar(Incubatorio entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId() == null) {
                prepadador = con.prepareStatement("INSERT INTO INCUBATORIO(IDLOTEOVOS, TEMPERATURA, UMIDADE, TEMPODECHOCAGEM, DATAINICIO, MORTALIDADE) VALUES (?, ?, ?, ?, ?, ?)");
            } else {
                prepadador = con.prepareStatement("UPDATE INCUBATORIO SET IDLOTEOVOS = ?, TEMPERATURA = ?, UMIDADE = ?, TEMPODECHOCAGEM = ?, DATAINICIO = ?, MORTALIDADE = ? WHERE IDINCUBATORIO = ?");
                prepadador.setInt(7, entidade.getId());
            }

            int idLote = new OvosDAO().buscarID(entidade.getLoteovos());
            prepadador.setInt(1, idLote);
            prepadador.setInt(2, entidade.getTemperatura());
            prepadador.setInt(3, entidade.getUmidade());
            prepadador.setInt(4, entidade.getTempochocar());
            prepadador.setDate(5, new Date(entidade.getDatainicio().getTime()));
            prepadador.setInt(6, entidade.getMortalidade());

            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar lote ovos!", ex);
        }
    }

    @Override
    public void deletar(Incubatorio entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("DELETE FROM INCUBATORIO WHERE IDINCUBATORIO = ?");
            prepadador.setInt(1, entidade.getId());
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar incubatorio!", ex);
        }
    }

    @Override
    public List<Incubatorio> buscar(Incubatorio entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM INCUBATORIO AS A JOIN OVOS AS B ON (A.IDLOTEOVOS = B.ID_OVOS) \n"
                    + "JOIN TIPOAVES AS C ON (B.ID_TIPOAVE = C.ID_TIPOAVES) WHERE 1 = 1");
            ResultSet rs = prepadador.executeQuery();
            List<Incubatorio> lista = new ArrayList<>();
            while (rs.next()) {
                Incubatorio inc = new Incubatorio();
                inc.setId(rs.getInt("IDINCUBATORIO"));
                inc.setDatainicio(rs.getDate("DATAINICIO"));
                inc.setLoteovos(rs.getString("OV_LOTE"));
                inc.setMortalidade(rs.getInt("MORTALIDADE"));
                inc.setTemperatura(rs.getInt("TEMPERATURA"));
                inc.setTempochocar(rs.getInt("TEMPODECHOCAGEM"));
                inc.setUmidade(rs.getInt("UMIDADE"));
                inc.setTipoAve(rs.getString("TA_NOME"));
                lista.add(inc);
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por ovos!", ex);
        }
    }
}
