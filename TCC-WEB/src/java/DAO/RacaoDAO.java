
package DAO;

import Entidades.Racao;
import Controle.Conexao;
import Controle.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RacaoDAO implements InterfaceCrudDAO<Racao>{

    @Override
    public void salvar(Racao entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador;

            if (entidade.getId() == null) {
                prepadador = con.prepareStatement("INSERT INTO RACAO(IDTIPORACAO, QUANTIDADERACAO, IDGRANJA, DATAINICIO, DATAFIM) VALUES (?, ?, ?, ?, ?)");
            } else {
                prepadador = con.prepareStatement("UPDATE RACAO SET IDTIPORACAO = ?, QUANTIDADERACAO = ?, IDGRANJA = ?, DATAINICIO = ?, DATAFIM = ?WHERE IDRACAO = ?");
                prepadador.setInt(6, entidade.getId());
            }
            
            prepadador.setInt(1, 1); // falta implementar busca
            prepadador.setDouble(2, entidade.getQuantidade());
            prepadador.setInt(3, 1); // falta emplementar busca
            prepadador.setDate(4, new Date(entidade.getDatainicio().getTime()));
            prepadador.setDate(5, new Date(entidade.getDatafim().getTime()));
            
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao salvar lote de ração!", ex);
        }
    }

    @Override
    public void deletar(Racao entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("DELETE FROM RACAO WHERE IDRACAO = ?");
            prepadador.setInt(1, entidade.getId());         
            prepadador.execute();
            prepadador.close();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar lote de ração!", ex);
        }
    }

    @Override
    public List<Racao> buscar(Racao entidade) throws ErroSistema {
        try {
            Connection con = Conexao.getConnection();
            PreparedStatement prepadador = con.prepareStatement("SELECT * FROM RACAO AS A JOIN TIPORACAO AS B ON (A.IDTIPORACAO = B.IDTIPORACAO) WHERE 1 = 1");
            ResultSet rs = prepadador.executeQuery();           
            List<Racao> lista = new ArrayList<>();
            while (rs.next()) {                
                Racao obj = new Racao();    
                obj.setId(rs.getInt("IDRACAO"));
                obj.setDatafim(rs.getDate("DATAFIM"));
                obj.setDatainicio(rs.getDate("DATAINICIO"));
                obj.setQuantidade(rs.getInt("QUANTIDADERACAO"));
                obj.setTiporacao(rs.getString("NOMERACAO"));
                lista.add(obj);
            }
            prepadador.close();
            return lista;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao fazer busca por Lote de Racão!", ex);
        }
    }
    
}
