
package DAO;

import Controle.ErroSistema;
import java.util.List;

public interface InterfaceCrudDAO<E> {
    
    public void salvar(E entidade) throws ErroSistema;
    
    public void deletar(E entidade) throws ErroSistema;
    
    public List<E> buscar(E entidade) throws ErroSistema;
}
