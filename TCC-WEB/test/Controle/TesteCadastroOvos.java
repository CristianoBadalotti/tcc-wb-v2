/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Entidades.Ovos;
import Bean.OvosBean;
import DAO.OvosDAO;
import Entidades.TipoAves;
import DAO.TipoAvesDAO;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crisf
 */
public class TesteCadastroOvos {

    public static void main(String[] args) {
        //testeCadastro();
        //testeAlterar();
        //testeDeletar();
        //mostrarTodos();
        //mostrarTodosAves();

        //mostrarTodosAvesID();
    }
    

    public static void mostraOvo(Ovos o) {
        System.out.println("Id: " + o.getId_ovos() + " Lote: " + o.getLote() + 
                    " Quatidade: " + o.getQuantidade_ovos() + " Data: " + o.getData_ponhagem_ovos().toString());
    }
    
    public static void testeAlterar() {
        Ovos ov = new Ovos();
        ov.setLote("12345678");
        ov.setData_ponhagem_ovos(new Date(2018, 05, 05));
        ov.setQuantidade_ovos(4000);
        ov.setIncubacao_ovos(true);
        ov.setId_ovos(2);
         
        OvosDAO ovDAO = new OvosDAO();
        try {
            ovDAO.salvar(ov);
        } catch (ErroSistema ex) {
            Logger.getLogger(TesteCadastroOvos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testeDeletar() {
        Ovos ov = new Ovos();
        ov.setId_ovos(1);
         
        OvosDAO ovDAO = new OvosDAO();
        try {
            ovDAO.deletar(ov);
        } catch (ErroSistema ex) {
            Logger.getLogger(TesteCadastroOvos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void mostrarTodos() {
        Ovos ov = new Ovos();
        
        OvosDAO ovDAO = new OvosDAO();
        List<Ovos> lista;
        try {
            lista = ovDAO.buscar(ov);
            
            for (Ovos o:lista) {
            System.out.println("Id: " + o.getId_ovos() + " Lote: " + o.getLote() + 
                    " Quatidade: " + o.getQuantidade_ovos() + " Data: " + o.getData_ponhagem_ovos().toString());
        }
        } catch (ErroSistema ex) {
            Logger.getLogger(TesteCadastroOvos.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
 
    
    public static void mostrarTodosAvesID() {
        TipoAvesDAO o = new TipoAvesDAO();
        try {
            System.out.println("Id: " + o.buscarID("PATO"));
        } catch (ErroSistema ex) {
            Logger.getLogger(TesteCadastroOvos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}
